# Estrutura básica de um projeto

> [Voltar para Linha do Tempo](../linha-do-tempo.md)

Um projeto no Django é constituído por uma ou mais aplicações. Cada aplicação visa resolver determinado problema, mas todas elas estão situadas em um mesmo projeto.

As aplicações possuem uma estrutura de pastas e arquivos pré-definida, como podemos ver na figura abaixo:

![Estrutura Projeto](imagens/estrutura-projeto.png)

Os arquivos presentes no diretório “clientes” representam uma aplicação. Suas funções são:

- **Migrations**: Este diretório é responsável por armazenar os arquivos de migração do projeto. São eles os responsáveis por criar a estrutura do banco de dados que utilizamos em nossas aplicações;
- **Admin.py**: Arquivo para gerência do módulo administrativo do Django. Não o veremos neste curso, mas é por meio deste arquivo que ativamos o módulo de administração do Django e determinamos quais entidades este pode gerenciar;
- **Apps.py**: Arquivo que contém as configurações da aplicação em questão;
- **Models.py**: É neste arquivo que definimos os models (Entidades) da nossa aplicação. É a partir dele que criamos as tabelas e os arquivos de migração (armazenados no diretório migrations) do projeto;
- **Tests.py**: Arquivo para desenvolvimento do módulo de testes do Django. Não entraremos em detalhes deste módulo neste curso;
- **Views.py**: Arquivo em que desenvolvemos os métodos de view do nosso projeto. É basicamente aqui que o “cérebro da aplicação” é desenvolvido.
