# O padrão MTV – Model-Template-View

> [Voltar para Linha do Tempo](../linha-do-tempo.md)

O Django segue o padrão arquitetural chamado MTV, que possuem três camadas (Model, Template e View). Esta arquitetura é bem similar ao MVC, utilizado na maioria dos frameworks web do mercado. Basicamente, suas responsabilidades são:

- O **Model** segue ao pé da letra a função que possui no padrão MVC, sendo responsável pela gestão de dados e lógica de negócio da aplicação. Possuindo um mapeador objeto-relacional (ORM), que permite definir os modelos sem utilizar códigos SQL (algo que também é possível, se você quiser).
- O **Template** é responsável pela criação das páginas HTML que exibem os dados para os usuários. É essa camada que faz a interação com o usuário.
- A **View** é responsável pelo “trâmite” de informações entre o model e o template. É essa camada que vai determinar quais dados serão retornados para o template. Em suma, é o “cérebro” da aplicação.
- Há também uma outra “camada” chamada de URLs. Basicamente, esta “camada” é responsável por direcionar para a view correta a depender da rota utilizada pelo usuário.

Um exemplo do funcionamento de uma aplicação Django pode ser vista abaixo:

- O usuário, através do Browser, acessa uma rota para obter determinado recurso;
- A URL invoca o método da view, que usa o model para obter os dados do banco de dados por meio do seu ORM;
- O model retorna os dados para a view, que usa o template (ou não) para retornar as informações para o usuário.

![Padrão MTV](imagens/padrao-mtv.png)
